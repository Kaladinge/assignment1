package no.noroff.accelerate.equipment;

import no.noroff.accelerate.attributes.HeroAttribute;

public class Armor extends Item{

    public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    // Fields
    protected ArmorType armortype;
    protected HeroAttribute ArmorAttribute;

    //Constructor
    public Armor() {
    }
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armortype,int strength, int dexterity, int intelligence) {
        super(name, requiredLevel);
        this.slot = slot;
        this.armortype = armortype;
        this.ArmorAttribute = new HeroAttribute(strength,dexterity,intelligence);
    }

    // Methods
    //Getters

    public ArmorType getArmorType() {
        return armortype;
    }
}
