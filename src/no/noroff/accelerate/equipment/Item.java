package no.noroff.accelerate.equipment;

public abstract class Item {
    public enum Slot {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }

    protected String name;
    protected int requiredLevel;
    protected Slot slot;

    // Constructors

    public Item() {
    }

    public Item (String name, int requiredLevel) {
        this.name = name;
        this.requiredLevel = requiredLevel;
    }


}
