package no.noroff.accelerate.equipment;

import java.util.ArrayList;

public class Weapon extends Item {

    public enum WeaponType {
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWORDS,
        WANDS
    }
    // Fields
    protected WeaponType weaponType;
    protected int WeaponDamage;

    // Constructor
    public Weapon() {
    }
    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(name, requiredLevel);
        this.slot = Slot.WEAPON;
        this.weaponType = weaponType;
        this.WeaponDamage = weaponDamage;
    }

    // Methods
    public String getWeaponName() {
        return name;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getRequiredLvl() {
        return requiredLevel;
    }
}


