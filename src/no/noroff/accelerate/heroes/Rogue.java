package no.noroff.accelerate.heroes;

import no.noroff.accelerate.attributes.HeroAttribute;
import no.noroff.accelerate.equipment.Armor;
import no.noroff.accelerate.equipment.Armor.ArmorType;
import no.noroff.accelerate.equipment.Item;
import no.noroff.accelerate.equipment.Item.Slot;
import no.noroff.accelerate.equipment.Weapon;
import no.noroff.accelerate.equipment.Weapon.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Rogue extends Hero {

    // Fields - state

    // Constructor

    public Rogue() {
    }

    public Rogue(String name) {
        super(name);
        this.levelAttributes = new HeroAttribute(2,6,1);
        this.ValidWeaponTypes = new ArrayList(Arrays.asList(WeaponType.DAGGERS, WeaponType.SWORDS));
        this.ValidArmourTypes = new ArrayList(Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL));
        this.equipment = new HashMap<>();
        this.equipment.put(Item.Slot.HEAD, null);
        this.equipment.put(Item.Slot.BODY, null);
        this.equipment.put(Item.Slot.LEGS, null);
        this.equipment.put(Item.Slot.WEAPON, null);
        this.level = 1;
    }

    // Methods
    public void lvlUp() {
        this.level++;
        levelAttributes.attributesUp(1, 4, 1);
    }

    public void equipItem(Item.Slot slot , Item item) {

        if(slot.equals(Item.Slot.WEAPON)) {
            setWeapon((Weapon) item, slot);
        }

        else {
            setArmor((Armor) item, slot);
        }
    }

    private void setWeapon(Weapon weapon, Item.Slot slot) {
        if (this.level > weapon.getRequiredLvl()) {
            if (ValidWeaponTypes.contains(weapon.getWeaponType())) {
                this.equipment.put(slot, (Item) weapon);
            } else {
                System.out.println("Invalid Weapon type");
            }
        } else {
            System.out.println("Not high enough mage level for this weapon");
        };
    }

    private void setArmor(Armor armor, Item.Slot slot) {
        if (ValidArmourTypes.contains(armor.getArmorType())) {
            this.equipment.put(slot, (Armor) armor);
        } else {
            System.out.println("Invalid Armor Type");
        }
    }

    public Item getWeapon() {
        return this.equipment.get(Item.Slot.WEAPON);
    }

    public String displayHero() {
        return "Name: " + name + "\n" + "Class: Rogue" + "\n" +
                "Level: " + level + "\n" + "Total Strength: " + levelAttributes.getStrength() +
                "\n" + "Total dexterity: " + levelAttributes.getDexterity() + "\n" +
                "Total Intelligence: " + levelAttributes.getIntelligence();
    }
}
