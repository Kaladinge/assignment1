package no.noroff.accelerate.heroes;

import no.noroff.accelerate.attributes.HeroAttribute;
import no.noroff.accelerate.equipment.Item;
import no.noroff.accelerate.equipment.Item.Slot;
import no.noroff.accelerate.equipment.Weapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public abstract class Hero {

    // Fields - state
    protected String name;
    protected int level;
    protected HeroAttribute levelAttributes;

    protected HashMap<Slot, Item> equipment;
    protected ArrayList<String> ValidWeaponTypes;

    protected ArrayList<String> ValidArmourTypes;

    // Constructors
    public Hero() {
    }

    public Hero(String name) {
        this.name = name;
    }

    // Methods
    public int getLevel() {return level;}



    public ArrayList<String> getValidWeaponTypes() {return ValidWeaponTypes;}

    public HashMap<Slot, Item> getEquipment() {return equipment;}

    public int getLevelAttributes() {return levelAttributes.getIntelligence();}
}
