package no.noroff.accelerate.heroes;

import no.noroff.accelerate.attributes.HeroAttribute;
import no.noroff.accelerate.equipment.Armor;
import no.noroff.accelerate.equipment.Armor.ArmorType;
import no.noroff.accelerate.equipment.Item;
import no.noroff.accelerate.equipment.Item.Slot;

import no.noroff.accelerate.equipment.Weapon;
import no.noroff.accelerate.equipment.Weapon.WeaponType;

import java.util.*;

public class Mage extends Hero {

    // Fields - state

    // Constructor
    public Mage() {
    }

    public Mage(String name) {
        super(name);
        this.levelAttributes = new HeroAttribute(1,1,8);
        // this.ValidWeaponTypes = List.of(new WeaponType[]{WeaponType.AXES, WeaponType.BOWS});
        this.ValidWeaponTypes = new ArrayList(Arrays.asList(WeaponType.STAFFS, WeaponType.WANDS));
        this.ValidArmourTypes = new ArrayList(Arrays.asList(ArmorType.CLOTH));
        this.equipment = new HashMap<>();
        this.equipment.put(Slot.HEAD, null);
        this.equipment.put(Slot.BODY, null);
        this.equipment.put(Slot.LEGS, null);
        this.equipment.put(Slot.WEAPON, null);
        this.level = 1;
    }

    // Methods
    public void lvlUp() {
        this.level++;
        levelAttributes.attributesUp(1, 1, 5);
    }

    public void equipItem(Slot slot , Item item) {

        if(slot.equals(Slot.WEAPON)) {
            setWeapon((Weapon) item, slot);
            }

        else {
            setArmor((Armor) item, slot);
        }
    }

    private void setWeapon(Weapon weapon, Slot slot) {
        if (this.level > weapon.getRequiredLvl()) {
            if (ValidWeaponTypes.contains(weapon.getWeaponType())) {
                this.equipment.put(slot, (Item) weapon);
            } else {
                System.out.println("Invalid Weapon type");
            }
        } else {
            System.out.println("Not high enough mage level for this weapon");
        };
    }

    private void setArmor(Armor armor, Slot slot) {
        if (ValidArmourTypes.contains(armor.getArmorType())) {
            this.equipment.put(slot, (Armor) armor);
        } else {
            System.out.println("Invalid Armor Type");
        }
    }


    public Item getWeapon() {
        return this.equipment.get(Slot.WEAPON);
    }

    public String displayHero() {
        return "Name: " + name + "\n" + "Class: Mage" + "\n" +
                "Level: " + level + "\n" + "Total Strength: " + levelAttributes.getStrength() +
                "\n" + "Total dexterity: " + levelAttributes.getDexterity() + "\n" +
                "Total Intelligence: " + levelAttributes.getIntelligence();
    }
}
