package no.noroff.accelerate.attributes;

public class HeroAttribute {
    // Fields - state
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    // Constructors
    public HeroAttribute() {
    }

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // Methods
    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void attributesUp( int strength, int dexterity, int intelligence ) {
        this.strength = this.strength + strength;
        this.dexterity = this.dexterity + dexterity;
        this.intelligence = this.intelligence + intelligence;
    }
}
