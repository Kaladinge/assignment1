package no.noroff.accelerate;

import no.noroff.accelerate.equipment.Item;
import no.noroff.accelerate.equipment.Weapon;
import no.noroff.accelerate.heroes.Mage;

public class Main {
    public static void main(String[] args) {

        Mage mage = new Mage("Kaladingus");
        Weapon staff = new Weapon("Fireball", 2, Weapon.WeaponType.STAFFS, 5);
        mage.equipItem(Item.Slot.WEAPON, staff);
        System.out.println(mage.displayHero());
        System.out.println(mage.getWeapon());
        System.out.println(staff.getWeaponName());
        mage.lvlUp();
        mage.getLevelAttributes();
        System.out.println(mage.getValidWeaponTypes());
        System.out.println(mage.getEquipment());
        System.out.println(mage.getLevel());
        System.out.println(mage.getLevelAttributes());
    }
}