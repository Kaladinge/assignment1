package no.noroff.accelerate.equipment;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
        @Test
        public void createWeapon() {
            //arrange
            Armor myArmor = new Armor("Daedra Helmet",5, Item.Slot.HEAD,
                    Armor.ArmorType.PLATE, 4, 3,2);
            String expectedName = "Daedra Helmet";
            int expectedLevel = 5;
            Item.Slot expectedSlot= Item.Slot.HEAD;
            Armor.ArmorType expectedArmorType = Armor.ArmorType.PLATE;
            int expectedStrength = 4;
            int expectedDexterity = 3;
            int expectedIntelligence = 2;

            //act
            String actualName = myArmor.name;
            int actualLevel = myArmor.requiredLevel;
            Item.Slot actualSlot = myArmor.slot;
            Armor.ArmorType actualArmorType = myArmor.armortype;
            int actualStrength = myArmor.ArmorAttribute.getStrength();
            int actualDexterity= myArmor.ArmorAttribute.getDexterity();
            int actualIntelligence = myArmor.ArmorAttribute.getIntelligence();
            //assert
            assertEquals(expectedName, actualName);
            assertEquals(expectedLevel, actualLevel);
            assertEquals(expectedSlot, actualSlot);
            assertEquals(expectedArmorType, actualArmorType);
            assertEquals(expectedStrength, actualStrength);
            assertEquals(expectedDexterity, actualDexterity);
            assertEquals(expectedIntelligence, actualIntelligence);

    }
}