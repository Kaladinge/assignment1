package no.noroff.accelerate.equipment;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    @Test
    public void createWeapon() {
        //arrange
        Weapon myWeapon = new Weapon("Ice Fang", 3, Weapon.WeaponType.SWORDS,4);
        String expectedName = "Ice Fang";
        int expectedLevel = 3;
        Weapon.WeaponType expectedWeapon = Weapon.WeaponType.SWORDS;
        int expectedDamage = 4;
        //act
        String actualName = myWeapon.name;
        int actualLevel = myWeapon.requiredLevel;
        Weapon.WeaponType actualWeapon = myWeapon.weaponType;
        int actualDamage = myWeapon.WeaponDamage;
        //assert
        assertEquals(expectedName, actualName);
        assertEquals(expectedLevel, actualLevel);
        assertEquals(expectedWeapon, actualWeapon);
        assertEquals(expectedDamage, actualDamage);
    }
}