package no.noroff.accelerate.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

     @Test
    public void createMage() {
         //arrange
         Mage myMage = new Mage("Olaf");
         String expectedName = "Olaf";
         int expectedLevel = 1;
         int expectedStrength = 1;
         int expectedDexterity = 1;
         int expectedIntelligence = 8;
         //act
         String actual = myMage.name;
         int actualLevel = myMage.level;
         int actualStrength = myMage.levelAttributes.getStrength();
         int actualDexterity = myMage.levelAttributes.getDexterity();
         int actualIntelligence = myMage.levelAttributes.getIntelligence();
         //assert
         assertEquals(expectedName, actual);
         assertEquals(expectedLevel, actualLevel);
         assertEquals(expectedStrength, actualStrength);
         assertEquals(expectedDexterity, actualDexterity);
         assertEquals(expectedIntelligence, actualIntelligence);
     }

     @Test
           public void levelUp() {
          //arr
          Mage myMage = new Mage("Olaf");
          myMage.lvlUp();
          int expectedLevel = 2;
          int expectedStrength = 2;
          int expectedDexterity = 2;
          int expectedIntelligence = 13;
          //act
          int actualLevel = myMage.level;
          int actualStrength = myMage.levelAttributes.getStrength();
          int actualDexterity = myMage.levelAttributes.getDexterity();
          int actualIntelligence = myMage.levelAttributes.getIntelligence();
          //assert
          assertEquals(expectedLevel,actualLevel);
          assertEquals(expectedStrength,actualStrength);
          assertEquals(expectedDexterity,actualDexterity);
          assertEquals(expectedIntelligence,actualIntelligence);
     }
}