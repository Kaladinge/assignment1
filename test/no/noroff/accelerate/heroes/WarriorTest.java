package no.noroff.accelerate.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    public void createWarrior() {
        //arrange
        Warrior myWarrior = new Warrior("Ulfric");
        String expectedName = "Ulfric";
        int expectedLevel = 1;
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;
        //act
        String actual = myWarrior.name;
        int actualLevel = myWarrior.level;
        int actualStrength = myWarrior.levelAttributes.getStrength();
        int actualDexterity = myWarrior.levelAttributes.getDexterity();
        int actualIntelligence = myWarrior.levelAttributes.getIntelligence();
        //assert
        assertEquals(expectedName, actual);
        assertEquals(expectedLevel, actualLevel);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void levelUp() {
        //arr
        Warrior myWarrior = new Warrior("Ulfric");
        myWarrior.lvlUp();
        int expectedLevel = 2;
        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;
        //act
        int actualLevel = myWarrior.level;
        int actualStrength = myWarrior.levelAttributes.getStrength();
        int actualDexterity = myWarrior.levelAttributes.getDexterity();
        int actualIntelligence = myWarrior.levelAttributes.getIntelligence();
        //assert
        assertEquals(expectedLevel,actualLevel);
        assertEquals(expectedStrength,actualStrength);
        assertEquals(expectedDexterity,actualDexterity);
        assertEquals(expectedIntelligence,actualIntelligence);
    }
}