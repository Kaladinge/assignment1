package no.noroff.accelerate.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    @Test
    public void createRogue() {
        //arrange
        Rogue myRogue = new Rogue("Uphi");
        String expectedName = "Uphi";
        int expectedLevel = 1;
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;
        //act
        String actual = myRogue.name;
        int actualLevel = myRogue.level;
        int actualStrength = myRogue.levelAttributes.getStrength();
        int actualDexterity = myRogue.levelAttributes.getDexterity();
        int actualIntelligence = myRogue.levelAttributes.getIntelligence();
        //assert
        assertEquals(expectedName, actual);
        assertEquals(expectedLevel, actualLevel);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void levelUp() {
        //arr
        Rogue myRogue = new Rogue("Uphi");
        myRogue.lvlUp();
        int expectedLevel = 2;
        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedIntelligence = 2;
        //act
        int actualLevel = myRogue.level;
        int actualStrength = myRogue.levelAttributes.getStrength();
        int actualDexterity = myRogue.levelAttributes.getDexterity();
        int actualIntelligence = myRogue.levelAttributes.getIntelligence();
        //assert
        assertEquals(expectedLevel,actualLevel);
        assertEquals(expectedStrength,actualStrength);
        assertEquals(expectedDexterity,actualDexterity);
        assertEquals(expectedIntelligence,actualIntelligence);
    }
}