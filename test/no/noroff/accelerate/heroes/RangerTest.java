package no.noroff.accelerate.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    @Test
    public void createRanger() {
        //arrange
        Ranger myRanger = new Ranger("Rangy");
        String expectedName = "Rangy";
        int expectedLevel = 1;
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;
        //act
        String actual = myRanger.name;
        int actualLevel = myRanger.level;
        int actualStrength = myRanger.levelAttributes.getStrength();
        int actualDexterity = myRanger.levelAttributes.getDexterity();
        int actualIntelligence = myRanger.levelAttributes.getIntelligence();
        //assert
        assertEquals(expectedName, actual);
        assertEquals(expectedLevel, actualLevel);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
    @Test
    public void levelUp() {
        //arr
        Ranger myRanger = new Ranger("Rangy");
        myRanger.lvlUp();
        int expectedLevel = 2;
        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedIntelligence = 2;
        //act
        int actualLevel = myRanger.level;
        int actualStrength = myRanger.levelAttributes.getStrength();
        int actualDexterity = myRanger.levelAttributes.getDexterity();
        int actualIntelligence = myRanger.levelAttributes.getIntelligence();
        //assert
        assertEquals(expectedLevel,actualLevel);
        assertEquals(expectedStrength,actualStrength);
        assertEquals(expectedDexterity,actualDexterity);
        assertEquals(expectedIntelligence,actualIntelligence);
    }
}