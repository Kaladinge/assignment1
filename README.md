# RPG game

This RPG game is a console application built in Java. Its concept is similar to that of D & D in which players can
make different hero classes and use these for battle. The heroes can equip different weapon and armor and are able to level
up upon getting experience.

## Installation

Download and install [Intellij](https://www.jetbrains.com/idea/promo/) and [JDK17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html) to run the application.

## Usage

```java
# creates a mage
Mage newMage = new Mage('Kaladingus')

# levels up the hero increasing its 'level' and 'attributes'
myMage.lvlup();

# returns fields of the hero (name, attributes, level, current equipment, possible weapon types, possible armor types) 
myMage.displayHero();

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
```
